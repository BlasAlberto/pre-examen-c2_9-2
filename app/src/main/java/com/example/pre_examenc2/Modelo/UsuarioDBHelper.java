package com.example.pre_examenc2.Modelo;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class UsuarioDBHelper extends SQLiteOpenHelper {
    private static final String TEXT_TYPE = " TEXT";
    private static final String CADENA_INT = " INTEGER";
    private static final String COMMA_SEP = ", ";

    private static final String
            SQL_CREATE_USUARIOS = "CREATE TABLE " +
            DefineTabla.Usuarios.TABLE_NAME + "( " +
            DefineTabla.Usuarios.COLUMN_NAME_ID + CADENA_INT + " PRIMARY KEY" + COMMA_SEP +
            DefineTabla.Usuarios.COLUMN_NAME_NOMBRE + TEXT_TYPE + COMMA_SEP +
            DefineTabla.Usuarios.COLUMN_NAME_CORREO + TEXT_TYPE + COMMA_SEP +
            DefineTabla.Usuarios.COLUMN_NAME_CONTRASENA + TEXT_TYPE + " );";

    private static final String
            SQL_DELETE_USUARIOS = "CREATE TABLE " +
            DefineTabla.Usuarios.TABLE_NAME;

    private static final String DATABASE_NAME = "sistema.db";

    private static final int DATABASE_VERSION = 1;

    public UsuarioDBHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_USUARIOS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(SQL_DELETE_USUARIOS);
        onCreate(sqLiteDatabase);
    }

}
