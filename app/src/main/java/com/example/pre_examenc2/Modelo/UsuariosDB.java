package com.example.pre_examenc2.Modelo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;

import com.example.pre_examenc2.Usuario;

import java.util.ArrayList;

public class UsuariosDB implements Persistencia, Proyeccion{

    private Context context;
    private UsuarioDBHelper helper;
    private SQLiteDatabase db;

    public UsuariosDB(Context context, UsuarioDBHelper helper){
        this.context = context;
        this.helper = helper;
    }
    public UsuariosDB(Context context){
        this.context = context;
        this.helper = new UsuarioDBHelper(this.context);

        this.openDataBase();
    }


    // ---------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------


    @Override
    public void openDataBase() {
        db = helper.getWritableDatabase();
    }
    @Override
    public void closeDataBase() {
        helper.close();
    }


    // ---------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------


    @Override
    public long insertUsuario(Usuario usuario)
    {
        ContentValues values = new ContentValues();

        values.put(DefineTabla.Usuarios.COLUMN_NAME_NOMBRE, usuario.getNombre());
        values.put(DefineTabla.Usuarios.COLUMN_NAME_CORREO, usuario.getCorreo());
        values.put(DefineTabla.Usuarios.COLUMN_NAME_CONTRASENA, usuario.getContrasena());

        this.openDataBase();
        long num = db.insert(DefineTabla.Usuarios.TABLE_NAME, null, values);
        //this.closeDataBase();
        Log.d("agregar", "insertAlumno" + num);

        return num;
    }
    @Override
    public long updateUsuario(Usuario usuario)
    {
        ContentValues values = new ContentValues();

        values.put(DefineTabla.Usuarios.COLUMN_NAME_NOMBRE, usuario.getNombre());
        values.put(DefineTabla.Usuarios.COLUMN_NAME_CORREO, usuario.getCorreo());
        values.put(DefineTabla.Usuarios.COLUMN_NAME_CONTRASENA, usuario.getContrasena());

        this.openDataBase();
        long num = db.update(
                DefineTabla.Usuarios.TABLE_NAME,
                values,
                DefineTabla.Usuarios.COLUMN_NAME_ID + " = " + usuario.getId(),
                null
        );
        //this.closeDataBase();
        Log.d("modificar", "updateAlumno" + num);

        return num;
    }
    @Override
    public void deleteUsuario(int id) {
        ContentValues values = new ContentValues();

        this.openDataBase();
        db.delete(DefineTabla.Usuarios.TABLE_NAME,
                DefineTabla.Usuarios.COLUMN_NAME_ID + "=?",
                new String[] {String.valueOf(id)});
        //this.closeDataBase();
    }


    // ---------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------


    @Override
    public Usuario getUsuario(String correo)
    {
        db = helper.getWritableDatabase();

        Cursor cursor = db.query(
                DefineTabla.Usuarios.TABLE_NAME,
                DefineTabla.Usuarios.REGISTRO,
                DefineTabla.Usuarios.COLUMN_NAME_CORREO + "=?",
                new String[] {correo},
                null, null, null);
        cursor.moveToFirst();

        Usuario usuario = new Usuario();
        if(cursor.getCount() > 0)
            usuario = readUsuario(cursor);

        return usuario;
    }
    @Override
    public ArrayList<Usuario> allUsuarios() {
        this.openDataBase();

        Cursor cursor = db.query(
                DefineTabla.Usuarios.TABLE_NAME,
                DefineTabla.Usuarios.REGISTRO,
                null, null, null, null, null);
        ArrayList<Usuario> usuarios = new ArrayList<>();
        cursor.moveToFirst();

        while(!cursor.isAfterLast()){
            Usuario usuario = readUsuario(cursor);
            usuarios.add(usuario);
            cursor.moveToNext();
        }
        cursor.close();

        return usuarios;
    }
    @Override
    public Usuario ultimoUsuario() {
        db = helper.getWritableDatabase();

        // Definir la consulta SQL
        String query =
                "SELECT * FROM " + DefineTabla.Usuarios.TABLE_NAME
                        + " ORDER BY " + DefineTabla.Usuarios.COLUMN_NAME_ID
                        + " DESC LIMIT 1";

        // Ejecutar la consulta
        Cursor cursor = db.rawQuery(query, null);

        cursor.moveToFirst();
        Usuario usuario = readUsuario(cursor);

        return usuario;

    }


    // ---------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------


    @Override
    public Usuario readUsuario(Cursor cursor) {
        Usuario usuario = new Usuario();

        usuario.setId(cursor.getInt(0));
        usuario.setNombre(cursor.getString(1));
        usuario.setCorreo(cursor.getString(2));
        usuario.setContrasena(cursor.getString(3));

        return usuario;
    }
}
