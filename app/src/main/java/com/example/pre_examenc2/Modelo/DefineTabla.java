package com.example.pre_examenc2.Modelo;

public class DefineTabla {

    public DefineTabla(){}

    public static abstract class Usuarios{
        public static final String TABLE_NAME = "usuarios";
        public static final String COLUMN_NAME_ID = "id";
        public static final String COLUMN_NAME_NOMBRE = "nombre";
        public static final String COLUMN_NAME_CORREO = "correo";
        public static final String COLUMN_NAME_CONTRASENA = "contrasena";

        public static String[] REGISTRO = new String[]{
                Usuarios.COLUMN_NAME_ID,
                Usuarios.COLUMN_NAME_NOMBRE,
                Usuarios.COLUMN_NAME_CORREO,
                Usuarios.COLUMN_NAME_CONTRASENA
        };
    }
}
