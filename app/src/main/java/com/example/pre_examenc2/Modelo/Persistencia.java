package com.example.pre_examenc2.Modelo;

import com.example.pre_examenc2.Usuario;

public interface Persistencia {
    public void openDataBase();
    public void closeDataBase();
    public long insertUsuario(Usuario usuario);
    public long updateUsuario(Usuario usuario);
    public void deleteUsuario(int id);
}
