package com.example.pre_examenc2.Modelo;

import android.database.Cursor;

import com.example.pre_examenc2.Usuario;

import java.util.ArrayList;

public interface Proyeccion {
    public Usuario getUsuario(String correo);
    public ArrayList<Usuario> allUsuarios();
    public Usuario readUsuario(Cursor cursor);

    public Usuario ultimoUsuario();
}
