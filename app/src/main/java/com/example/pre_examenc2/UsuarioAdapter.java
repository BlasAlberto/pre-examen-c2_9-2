package com.example.pre_examenc2;

import android.app.Application;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class UsuarioAdapter extends RecyclerView.Adapter<UsuarioAdapter.ViewHolder> implements View.OnClickListener {

    // Lista con la información de los usuarios
    private ArrayList<Usuario> infUsuarios;
    private Context context;
    private View.OnClickListener listener;


    // ---------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------


    public UsuarioAdapter(ArrayList<Usuario> infUsuarios, Application contexto)
    {
        this.infUsuarios = infUsuarios;
        this.context = contexto;
    }


    // ---------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView textView;

        public ViewHolder(View itemView) {
            super(itemView);
            this.textView = itemView.findViewById(android.R.id.text1);
        }
    }


    // ---------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(android.R.layout.simple_list_item_1, parent, false);
        return new ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position)
    {
        Usuario usuario = this.infUsuarios.get(position);

        holder.textView.setText(usuario.getNombre());
        if (listener != null)
            holder.textView.setOnClickListener(this.listener);

    }
    @Override
    public int getItemCount() {
        return this.infUsuarios.size();
    }


    // ---------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------


    @Override
    public void onClick(View view) {

    }
    public void setOnClickListener(View.OnClickListener listener){
        this.listener = listener;
        Toast.makeText(context, "Lito", Toast.LENGTH_SHORT).show();
    }
}
