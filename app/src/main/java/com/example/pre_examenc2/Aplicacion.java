package com.example.pre_examenc2;

import android.app.Application;
import android.util.Log;
import android.widget.Toast;

import com.example.pre_examenc2.Modelo.UsuariosDB;

import java.util.ArrayList;

public class Aplicacion extends Application
{
    public static ArrayList<Usuario> usuarios;
    public static UsuariosDB usuarioDB;

    public static UsuarioAdapter adaptador;

    @Override
    public void onCreate() {
        super.onCreate();

        this.usuarioDB = new UsuariosDB(getApplicationContext());
        this.usuarios = usuarioDB.allUsuarios();


        this.adaptador = new UsuarioAdapter(this.usuarios, this);
    }

    public ArrayList<Usuario> getAlumnos() { return this.usuarios; }
    public static UsuarioAdapter getAdaptador() { return adaptador; }
}
