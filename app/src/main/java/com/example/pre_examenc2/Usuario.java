package com.example.pre_examenc2;

import java.io.Serializable;

public class Usuario implements Serializable {

    // ATRIBUTOS
    private int id;
    private String nombre;
    private String correo;
    private String contrasena;


    // CONSTRUCTORES
    public Usuario(){
        this.id = 0;
        this.nombre = "";
        this.correo = "";
        this.contrasena = "";
    }
    public Usuario(int id, String nombre, String correo, String contrasena){
        this.id = id;
        this.nombre = nombre;
        this.correo = correo;
        this.contrasena = contrasena;
    }


    // GETs & SETs (ENCAPSULAMIENTO)
    public int getId() { return id; }
    public void setId(int id) { this.id = id; }

    public String getNombre() { return nombre; }
    public void setNombre(String nombre) { this.nombre = nombre; }

    public String getCorreo() { return correo; }
    public void setCorreo(String correo) { this.correo = correo; }

    public String getContrasena() { return contrasena; }
    public void setContrasena(String contrasena) { this.contrasena = contrasena; }
}
