package com.example.pre_examenc2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class listaActivity extends AppCompatActivity {

    private RecyclerView rvUsuarios;
    private RecyclerView.LayoutManager layoutManager;

    private Button btnRegresar;


    // ---------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);

        this.instanciarComponentes();
        rvUsuarios.getAdapter().notifyDataSetChanged();

        Aplicacion.getAdaptador().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { clic_elementos(view); }
        });
        this.btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { click_btnRegresar(); }
        });
    }

    private void instanciarComponentes()
    {
        this.layoutManager = new LinearLayoutManager(this);

        this.rvUsuarios = findViewById(R.id.rvUsuarios);
        this.rvUsuarios.setAdapter(Aplicacion.getAdaptador());
        this.rvUsuarios.setLayoutManager(this.layoutManager);

        this.btnRegresar = findViewById(R.id.btnRegresar);
    }

    private void clic_elementos(View view)
    {
        int posicion = rvUsuarios.getChildAdapterPosition(view);
        Usuario usr = Aplicacion.usuarios.get(posicion);

        Toast.makeText(listaActivity.this, "" + usr.getCorreo(), Toast.LENGTH_SHORT).show();
    }

    private void click_btnRegresar(){
        finish();
    }
}