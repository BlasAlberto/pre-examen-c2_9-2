package com.example.pre_examenc2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class RegistroActivity extends AppCompatActivity {

    private EditText txtUsuario;
    private EditText txtCorreoElectronico;
    private EditText txtContraseña;
    private EditText txtContraseña2;
    private Button btnRegistrar;
    private Button btnRegresar;
    private Button btnLimpiar;


    // ---------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        this.instanciarComponentes();
    }

    // RELACIONAR LOS COMPONENTES CON EL LAYOUT
    private void instanciarComponentes()
    {
        this.txtUsuario = (EditText) findViewById(R.id.txtNombreUsuario);
        this.txtCorreoElectronico = (EditText) findViewById(R.id.txtCorreoElectronico);
        this.txtContraseña = (EditText) findViewById(R.id.txtRegContrasena);
        this.txtContraseña2 = (EditText) findViewById(R.id.txtRegContrasena2);

        this.btnRegistrar = (Button) findViewById(R.id.btnMainRegistrar);
        this.btnRegresar = (Button) findViewById(R.id.btnRegresar);
        this.btnLimpiar = (Button) findViewById(R.id.btnLimpiarRegistrar);

        this.btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { clic_btnRegistrar(); }
        });
        this.btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { clic_btnRegresar(); }
        });
        this.btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { clic_btnLimpiar(); }
        });
    }

    public boolean validarCampos()
    {
        String strUsuario = this.txtUsuario.getText().toString();
        String strCorreo = this.txtCorreoElectronico.getText().toString();
        String strContrasena = this.txtContraseña.getText().toString();
        String strContrasena2 = this.txtContraseña2.getText().toString();

        if( strUsuario.equals("") ||
            strCorreo.equals("") ||
            strContrasena.equals("") ||
            strContrasena2.equals("")
        )
            return false;

        else
            return true;
    }


    // ---------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------


    private void clic_btnRegistrar()
    {
        // Validación
        // Cancelar proceso si no se ingreso todos los campos
        if (!this.validarCampos())
        {
            Toast.makeText(this, "Favor de ingresar todos los campos.", Toast.LENGTH_SHORT).show();
            return;
        }

        // Obtener contraseñas
        String strContrasena = this.txtContraseña.getText().toString();
        String strContrasena2 = this.txtContraseña2.getText().toString();

        // Validación
        // Cancelar proceso si las contraseñas no son las mismas
        if (!strContrasena2.equals(strContrasena))
        {
            Toast.makeText(this, "Las contraseñas no son las mismas", Toast.LENGTH_SHORT).show();
            return;
        }

        // Obtener los demas datos
        String strUsuario = this.txtUsuario.getText().toString();
        String strCorreo = this.txtCorreoElectronico.getText().toString();

        // Crear nuevo usuario
        Usuario usr = new Usuario(0, strUsuario, strCorreo, strContrasena);

        // Agregar usuario a la base de datos
        Aplicacion.usuarioDB.insertUsuario(usr);

        Usuario nuevoAlumno = Aplicacion.usuarioDB.ultimoUsuario();
        Aplicacion.usuarios.add(nuevoAlumno);

        finish();
    }
    private void clic_btnRegresar(){
        finish();
    }
    private void clic_btnLimpiar(){
        this.txtUsuario.setText("");
        this.txtCorreoElectronico.setText("");
        this.txtContraseña.setText("");
        this.txtContraseña2.setText("");
    }
}