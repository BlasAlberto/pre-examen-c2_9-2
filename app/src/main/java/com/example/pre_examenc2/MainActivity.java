package com.example.pre_examenc2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText txtCorreoElectronico;
    private EditText txtContrasena;
    private Button btnIngresar;
    private Button btnRegistrar;
    private Button btnLimpiar;

    private Aplicacion app;


    // ---------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.instanciarComponentes();

        this.app = (Aplicacion) getApplication();
    }

    // RELACIONAR LOS COMPONENTES CON EL LAYOUT
    public void instanciarComponentes()
    {
        this.txtCorreoElectronico = (EditText) findViewById(R.id.txtCorreoElectronico);
        this.txtContrasena = (EditText) findViewById(R.id.txtMainContrasena);

        this.btnIngresar = (Button) findViewById(R.id.btnMainIngresar);
        this.btnRegistrar = (Button) findViewById(R.id.btnActRegistrar);
        this.btnLimpiar = (Button) findViewById(R.id.btnLimpiarIngresar);

        this.btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { clic_btnIngresar(); }
        });
        this.btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { clic_btnRegistrar(); }
        });
        this.btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { clic_btnLimpiar(); }
        });

    }

    public boolean validarCampos()
    {
        String strCorreo = this.txtCorreoElectronico.getText().toString();
        String strContrasena = this.txtContrasena.getText().toString();

        if(strCorreo.equals("") || strContrasena.equals(""))
            return false;

        else
            return true;
    }


    // ---------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------


    public void clic_btnIngresar()
    {
        // Validación
        // Cancelar proceso si no se ingreso todos los campos
        if (!this.validarCampos())
        {
            Toast.makeText(this, "Favor de ingresar todos los campos.", Toast.LENGTH_SHORT).show();
            return;
        }

        // Obtener datos
        String strCorreo = this.txtCorreoElectronico.getText().toString();
        String strContrasena = this.txtContrasena.getText().toString();

        // Obtener alumno
        Usuario usr = app.usuarioDB.getUsuario(strCorreo);
        // Usuario usr = new Usuario();

        // Comprobación
        // Cancelar proceso si el usuario no es correcto
        if(!(usr.getCorreo().equals(strCorreo) &&
           usr.getContrasena().equals(strContrasena)))
        {
            Toast.makeText(this, "Usuario o contraseña incorrecta.", Toast.LENGTH_SHORT).show();
            return;
        }

        // Mandando el usuario que ingreso
        Bundle paquete = new Bundle();
        paquete.putSerializable("usuario", usr);

        // Iniciar la actividad
        Intent intent = new Intent(MainActivity.this, listaActivity.class);
        intent.putExtras(paquete);
        startActivityForResult(intent, 0);
    }
    public void clic_btnRegistrar()
    {
        this.clic_btnLimpiar();

        Intent intent = new Intent(MainActivity.this, RegistroActivity.class);
        startActivityForResult(intent, 0);
    }
    public void clic_btnLimpiar()
    {
        this.txtCorreoElectronico.setText("");
        this.txtContrasena.setText("");
    }
}